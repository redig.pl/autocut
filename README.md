# AutoCUt

AutoCUt for tacotron 2

## Getting started

Instalacja deepSpeach
pip3 install deepspeech
-- jak by były problem z nump  -> pip install numpy==1.14

Pobierz polski  modele do Deepspacha:
https://drive.google.com/drive/folders/1_hia1rRmmsLRrFIHANH4254KKZhY3p1c?usp=sharing
-- deepSpeach musi być w mono oraz 16000 "1_podziel.php" właśnie to zmienia

vst instalacja
https://github.com/8tm/vst/wiki

mobi to text - gdy masz text z audiobooka zapisz w audiobook.txt
https://www.zamzar.com/convert/mobi-to-txt/

Wymagania
php,ffmpeg, vst lub deepSpeach


Jak to działa
-- dodaj plik input.mp3 (użyj Audacity) można zmienić w pliku 1_podziel.php linia z exec
-- uruchom php 1_podziel.php pokroi wtedy audiobooka na małę cześci
-- jeżeli nie chcesz użyć deepSpeach albo vst zmień nazwe pliku done_template.txt na done.txt i pomiń następny krok
-- następnie "php 2.VST.php" lub "php 2.deepspeech.php" 
-- uruchom server www (Apache) lub StronaWWW.bat wejdź na stronę http://localhost:8000/
-- użyj klawisza tylda do skopiowania zaznaczonego tekstu do aktywnego pola klawisza "1" do przejśćia do nastepnego pola oraz "2" do ponownego odegrania dzwięku
-- po zakończeniu transkrybcji podmień plik done.txt
-- uruchom php out_final_do_tactrona.php który utworzy nowe audio w folderze "out_final" w mono oraz 22050
-- jeżeli nie korzystasz z deepSpeach to możesz się pokusic o edycje pliku 1_podziel.php i użycie exec "for tactron 2" linia 25, wtedy audio będzie na 22050



## License
MIT
